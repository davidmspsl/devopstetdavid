package ie.eightwest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Collection;
import java.util.Locale;

@Controller
public class HomeController {

    @Autowired
    private UserDao udb;

    @RequestMapping(method = RequestMethod.GET, value = "/homeoriginal")
    public String home(Locale locale, Model model){

        try {
            User user = udb.getUser(2);
            model.addAttribute("user", user);
            model.addAttribute("title", "test title");

        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        //this is a name of the view
        return "home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/userlist")
    public String getUsers(Model model){
        Collection<User> users = udb.getUsers();
        model.addAttribute("users", users);
        return "userlist";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/userdetail/{id}")
    public String showUserDetails(@PathVariable long id, Model model){
        System.out.println("show user called, id: "+id);
        try {
            //get user from db
            User user = udb.getUser(id);
            //add user to the model
            model.addAttribute(user);

        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        //return the view
        return "userdetail";
    }
}
