package ie.eightwest;


import java.util.*;


public class UserDaoInMemory implements UserDao{

    protected HashMap<Long, User> userMap;

    public UserDaoInMemory() {
        this.userMap = new HashMap<>();
    }

    public void deleteUser(long id) throws UserDaoException{
        if (userMap.containsKey(id)){
            userMap.remove(id);
        }
        else{
            //tbd throw exception
            throw new UserDaoException("user not found - can not be deleted");
        }
    }

    public User updateUser(User user) throws UserDaoException{
        if (userMap.containsKey(user.getId())){
            userMap.replace(user.getId(), user);
            return user;
        }
        else{
            throw new UserDaoException("User not found");
        }
    }

    public User addUser(User user) throws UserDaoException {
        if (user.getId() == -1){
            //assign a new id and add to map
            Set<Long> keys = userMap.keySet();
            //id with the highest value
            Long maxValue = keys.stream()
                    .max(Comparator.comparing(Long::valueOf))
                    .get();

//            Long maxValue = keys.stream()
//                    .max(Comparator.comparing((Long l)->{
//                        return Long.valueOf(l);
//                    }))
//                    .get();
            user.setId(maxValue+1);
            System.out.println("max value: "+maxValue);

        }
        else{
            if (userMap.containsKey(user.getId())){
                // id already exist throw exception
                throw new UserDaoException("user already exist, can not be added");
            }
        }
        // add this user with this id to the map
        userMap.put(user.getId(), user);
        return user;
    }

    public Collection<User> getUsers(){
        return userMap.values();
    }

    public User getUser(long id) throws UserDaoException{
        if (userMap.containsKey(id)){
            return userMap.get(id);
        }
        else {
            //should throw an exception
            throw new UserDaoException("user do not exist can not be returned");
        }
    }

    @Override
    public void close() throws UserDaoException {
        userMap.clear();
    }

    @Override
    public String toString() {
        return "UserDaoInMemory{" +
                "userMap=" + userMap +
                '}';
    }

    //----------------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {

        UserDao udb = new UserDaoInMemory();
        User user = new User(1,"user1", "email1", true);
        try {
            udb.addUser(user);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            udb.addUser(new User(2,"user2", "email2", true));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(udb);

        System.out.println("------------------ALL USERS---------------------------------");

        Collection<User> users = udb.getUsers();
        for (User u:users){
            System.out.println(u);
        }

        System.out.println("\n-------------TRYING TO FIND USERS WITH IDs: 1 and 99--------------------");

        User found = null;
        try {
            found = udb.getUser(1);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(found);
        User found2 = null;
        try {
            found2 = udb.getUser(99);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(found2);

        System.out.println("\n------------------DELETING USER1---------------------------------");
        try {
            udb.deleteUser(99);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(udb.getUsers());

        System.out.println("\n---------------UPDATING USER---------------------");
        User toBeUpdated = new User(4, "user4", "user4email", true);
        try {
            udb.addUser(toBeUpdated);
            udb.addUser(toBeUpdated);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        toBeUpdated.setName("changedUser4");
        try {
            udb.updateUser(toBeUpdated);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(udb.getUser(4));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println("---------ADDING USER WITHOUT ID------------------");
        User userNoId = new User("userWIthNoId", "email", true);
        try {
            udb.addUser(userNoId);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(udb.getUsers());


        User n2 = new User(99, "another new user", "test email", true);
        try {
            n2 = udb.addUser(n2);
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        System.out.println(udb.getUsers());
    }
}
