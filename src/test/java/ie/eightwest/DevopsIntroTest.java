package ie.eightwest;

import org.junit.Assert;
import org.junit.Test;

public class DevopsIntroTest {

    @Test
    public void testMessage() {
        Assert.assertEquals(DevopsIntro.message, "Devops Test");
    }

    @Test
    public void dodgyTest() {

        if (Math.random() < 0.33) {
            Assert.fail("Fails 1/3 pf the time");
        }else{
            Assert.assertEquals(DevopsIntro.message, "Devops Test");
        }
    }
}
